//
//  ViewController.swift
//  chat_estalim
//
//  Created by Ahmed Adm on 24/07/2020.
//  Copyright © 2020 Ahmed Adm. All rights reserved.
//

import UIKit



class ViewController: UIViewController, UITableViewDelegate , UITableViewDataSource {

    // https://blog.3d-logic.com/2019/07/28/swift-client-for-the-asp-net-core-version-of-signalr-part-1-getting-started/
    
    // https://github.com/moozzyk/SignalR-Client-Swift/blob/master/Examples/HubSamplePhone/ViewController.swift
    
    // http://astalem.com:8080/chatHubs
    
    
    // ReceiveMessage
    // Message, otherUserId, DateTimeNow, MessageType
    

     @IBOutlet weak var chatTableView: UITableView!

    
    override func viewDidLoad() {
       super.viewDidLoad()
       navigationController?.navigationBar.prefersLargeTitles = true
       navigationItem.largeTitleDisplayMode = .always
        
       self.chatTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
       self.chatTableView.delegate = self
       self.chatTableView.dataSource = self
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1// count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let row = indexPath.row
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = "Ahmed Adm Chat"//self.messages[row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        // chat messages
        var vc = ChatViewController()
        vc.title = "chat"
        navigationController?.pushViewController(vc, animated: true)
        
    }

}


