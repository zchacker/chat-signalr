//
//  ChatViewController.swift
//  chat_estalim
//
//  Created by Ahmed Adm on 26/07/2020.
//  Copyright © 2020 Ahmed Adm. All rights reserved.


// The library i used here in this class for SignalR you can find it here:
// https://github.com/moozzyk/SignalR-Client-Swift

// and for the Message View i used MessageKit
// https://github.com/MessageKit/MessageKit

import Foundation
import SwiftSignalRClient
import UIKit
import MessageKit
import InputBarAccessoryView
import SDWebImage
//import MessageInputBar


// this is my custom structs
struct MsgList:Decodable {
    var messages:[OneMessage]?
}

struct OneMessage:Decodable {
    var id:Int!
    var message:String!
    var userPhone:String!
    var messageType:String!
    var otherUserPhone:String!
    var dateTimeNow:String!
}

struct ServerResponseUploadImage : Decodable{
    var message:String?
    var error:String?
}

// this structs for MessageKit Module
struct Sender: SenderType{
   var senderId: String
   var displayName: String
}

struct Message: MessageType {
   var sender: SenderType
   var messageId: String
   var sentDate: Date
   var kind: MessageKind
   
}

struct Media: MediaItem{
    var url: URL?
    var image: UIImage?
    var placeholderImage: UIImage
    var size: CGSize
}

class ChatViewController: MessagesViewController, MessagesDataSource, MessagesDisplayDelegate, MessagesLayoutDelegate, InputBarAccessoryViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
   
    var user_image: UIImage!
    var totalMessages = 0
    var imagePcker:UIImagePickerController = UIImagePickerController.init()
    var iamgeName:String = ""
    
    var get_messages = "http://astalem.com:8080/api/Chat/GetMyChat?userPhone=0532418301&otherUserPhone=0500000000"
    var send_message = "http://astalem.com:8080/api/Chat/SendMessage"
    var upload_image = "http://astalem.com:8080/api/Chat/UploadImage"
    
    private let serverUrl = "http://astalem.com:8080/chatHubs"
    private var chatHubConnection: HubConnection?
    private var chatHubConnectionDelegate: HubConnectionDelegate?
    
    /* * * * * * * * * PLEASE CHANGE THIS VARIBALES * * * * * * * * * * * * * */
    // MARK: you should edit this attributes !!!!!!
    var current_user_id:String        = "0532418301"// this is my id
    var other_user_id:String          = "0500000000"// this is other user id
    
    var messageId       = 5
    let current_user    = Sender(senderId: "sender", displayName: "")
    let other_user      = Sender(senderId: "other", displayName: "")
    //let other_user2     = Sender(senderId: "other2", displayName: "mohammed")
    
    var messages        = [MessageType]()
    var messagesList    = [OneMessage]() // MsgList()
    
    private var reconnectAlert: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.largeTitleDisplayMode = .always
        maintainPositionOnKeyboardFrameChanged = true
        
        
        self.get_messages = "http://astalem.com:8080/api/Chat/GetMyChat?userPhone=\(self.current_user_id)&otherUserPhone=\(self.other_user_id)"
        messageInputBar.leftStackView.alignment = .center
        messageInputBar.setLeftStackViewWidthConstant(to: 50 , animated: false)
        
        self.setAttachmentManager(active: true)
                
        messageInputBar.delegate = self
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        
        self.connect_to_server()// this to connect to server
        self.read_messages()// get past messages from server
        
    }
    
    func connect_to_server(){
        
        self.chatHubConnectionDelegate = ChatHubConnectionDelegate(controller: self)
        
        /*
         [1:01 AM, 7/23/2020] مهندس محمد عباس: SaveUserConnectionId اسم حفظ المفتاح
         [1:02 AM, 7/23/2020] مهندس محمد عباس: userId البراميتر حقها
         */
        
        self.chatHubConnection = HubConnectionBuilder(url: URL(string: self.serverUrl)!)
            .withLogging(minLogLevel: .debug)
            .withAutoReconnect()
            .withHubConnectionDelegate(delegate: self.chatHubConnectionDelegate!)
            .build()
        self.chatHubConnection!.start()
        
        
        // save the user id to this connection
        chatHubConnection?.invoke(method: "SaveUserConnectionId", self.current_user_id , resultType: String.self) { result, error in
            if let error = error {
                print("error in hub: \(error)")
            } else {
                print("Add result: \(result!)")
            }
        }
        
        // Message, otherUserId, DateTimeNow, MessageType
        self.chatHubConnection!.on(method: "ReceiveMessage", callback: {( message: String, otherUserId: String, DateTimeNow: String, MessageType: String) in
            print("we get message: \(message)")
            
            self.append_one_message(message: message, otherUserId: otherUserId, DateTimeNow: DateTimeNow, MessageType: MessageType)
            
        })
        
    }
    
    func currentSender() -> SenderType {
        return current_user
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return self.messages[indexPath.section]
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return self.messages.count
    }

    // for input text
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        
        // send post request to server
        let jsonObject: [String: Any] = [
            "Message": text,
            "UserPhone": "\(self.current_user_id)",
            "MessageType": "text",
            "OtherUserPhone": "\(self.other_user_id)",
            "DateTimeNow": "2020-07-26T20:49:11.053Z"
        ]
        
        let valid = JSONSerialization.isValidJSONObject(jsonObject) // true
        
        let jsonData: NSData

        do {
            
            jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            
            // send json contain the message to server
            self.send_message_post(json: jsonString)

        } catch _ {
            print ("JSON Failure")
        }
        
        self.messageId += 1
        
        self.messages.append(Message(sender: current_user,
                                     messageId: "\(self.messageId)",
                                     sentDate: Date().addingTimeInterval(-86400),
                                     kind: .text(text)))
        
        messageInputBar.inputTextView.text = String()
        messagesCollectionView.reloadData()
        messagesCollectionView.scrollToBottom(animated: true)
        messageInputBar.invalidatePlugins()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.prefersLargeTitles = true
        chatHubConnection?.stop()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func connectionDidOpen() {
        //toggleUI(isEnabled: true)
        // save the user id to this connection
        chatHubConnection?.invoke(method: "SaveUserConnectionId", self.current_user_id  ){ error in
            if let error = error {
                print("error in SaveUserConnectionId: \(error)")
            }
        }
        
    }

    fileprivate func connectionDidFailToOpen(error: Error) {
        //blockUI(message: "Connection failed to start.", error: error)
    }

    fileprivate func connectionDidClose(error: Error?) {
        if let alert = reconnectAlert {
            alert.dismiss(animated: true, completion: nil)
        }
        //blockUI(message: "Connection is closed.", error: error)
    }

    fileprivate func connectionWillReconnect(error: Error?) {
        guard reconnectAlert == nil else {
            print("Alert already present. This is unexpected.")
            return
        }
        
        
        reconnectAlert = UIAlertController(title: "Reconnecting...", message: "Please wait", preferredStyle: .alert)
        self.present(reconnectAlert!, animated: true, completion: nil)
    }

    fileprivate func connectionDidReconnect() {
        reconnectAlert?.dismiss(animated: true, completion: nil)
        reconnectAlert = nil
    }

    func send_message_post(json:String){
                                
        let requestBody = json.data(using: .utf8) //parameters.percentEncoded()
        let url = URL(string: self.send_message)
        var request : URLRequest = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("*/*;", forHTTPHeaderField: "Accept")
        request.httpBody = requestBody;

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data,
            let response = response as? HTTPURLResponse,
            error == nil else { // check for fundamental networking error
                print("error", error ?? "Unknown error")
                return
            }
            
            guard (200 ... 299) ~= response.statusCode else {// check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }
            
            if let responseString = String(data: data, encoding: .utf8) {
                print("server response : \(responseString)")
            }
            
        }
        
        task.resume()
            
    }
    
    // send image json data to server
    func send_image_json_to_server(imageName:String){
        
        // send post request to server
        let jsonObject: [String: Any] = [
            "Message": imageName,
            "UserPhone": "\(self.current_user_id)",
            "MessageType": "image",
            "OtherUserPhone": "\(self.other_user_id)",
            "DateTimeNow": "2020-07-26T20:49:11.053Z"
        ]
        
        let valid = JSONSerialization.isValidJSONObject(jsonObject) // true
        
        let jsonData: NSData

        do {
            
            jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            
            // send json contain the message to server
            self.send_message_post(json: jsonString)
            DispatchQueue.main.async {
                self.messageId += 1
                
                var photo:Media = Media(
                    url: nil,
                    image: self.user_image,
                    placeholderImage: self.user_image,
                    size: CGSize(width: self.user_image.size.width  , height: self.user_image.size.height))
                
                self.messages.append(
                    Message(
                        sender: self.current_user,
                        messageId: "\(self.messageId)",
                        sentDate: Date().addingTimeInterval(-86400),
                        kind: .photo(photo)
                    )
                )
                
                self.messageInputBar.inputTextView.text = String()
                self.messagesCollectionView.reloadData()
                self.messagesCollectionView.scrollToBottom(animated: true)
                self.messageInputBar.invalidatePlugins()
            }
            
        } catch _ {
            print ("JSON Failure")
        }
        
        
    }

    // append one message
    func append_one_message(message: String, otherUserId: String, DateTimeNow: String, MessageType: String){
        if MessageType == "text" {
            
            var user:Sender!
            
            if otherUserId == self.other_user_id {
                user = self.other_user
            }else{
                user = self.current_user
            }
            
            self.messages.append(
                Message(
                    sender: user,
                    messageId: "1",
                    sentDate: Date().addingTimeInterval(-86400),
                    kind: .text(message)
                )
            )
            
        } else {
        
            if URL.init(string: "http://astalem.com:8080/upload/\(message)") != nil {
                DispatchQueue.global(qos: .background).async {
                    do{
                        let data = try Data.init(contentsOf: URL.init(string: "http://astalem.com:8080/upload/\(message)")!)
                        let image: UIImage = UIImage(data: data)! // UIImage(data: data)!
                        
                        DispatchQueue.main.async {
                            
                            var photo:Media = Media(
                                url: nil,
                                image: image ,
                                placeholderImage: UIImage() ,
                                size: CGSize(width: image.size.width, height: image.size.height)
                            )
                            
                            var user:Sender!
                            if otherUserId == self.other_user_id {
                                user = self.other_user
                            }else{
                                user = self.current_user
                            }
                            
                            self.messages.append(
                                Message(
                                    sender: user,
                                    messageId: "1",
                                    sentDate: Date().addingTimeInterval(-86400),
                                    kind: .photo(photo)
                                )
                            )
                            
                            self.messagesCollectionView.reloadData()
                            self.messagesCollectionView.scrollToBottom(animated: true)
                            
                        }
                    }catch{
                        print("error in adding message: \(error)")
                    }
                }
            }
        
        }
        
        DispatchQueue.main.async {
            self.messagesCollectionView.reloadData()
            self.messagesCollectionView.scrollToBottom(animated: true)
        }
    }
    
    // download messages
    func read_messages(){
        DispatchQueue.global(qos: .background).async {
            
            let url = URL(string: self.get_messages)
            
            
            let loading:UIAlertController = UIAlertController(title: "Loading...", message: "Please wait", preferredStyle: .alert)
            
            DispatchQueue.main.async {
                // MARK: disable blocking UI when loading messages
                //self.present(loading, animated: true, completion: nil)
                self.title = "Loading ..."
            }
            
            
            URLSession.shared.dataTask(with: url!){(data , response , error) in
                
                if let responseString = String(data: data!, encoding: .utf8) {
                    //print("server response : \(responseString)")
                }
                
                do{
                    
                    self.messagesList = try JSONDecoder().decode([OneMessage].self, from: data!)
                    print("get all messages: \(self.messagesList.count)")
                    // loop throw the struct array
                    
                    for msg in self.messagesList {
                        
                        if msg.messageType! == "text" {
                            
                            var user:Sender!
                            
                            if msg.userPhone! == self.current_user_id {
                                user = self.current_user
                            }else{
                                user = self.other_user
                            }
                            
                            self.messages.append(
                                Message(
                                    sender: user,
                                    messageId: "\(msg.id!)",
                                    sentDate: Date().addingTimeInterval(-86400),
                                    kind: .text(msg.message!)
                                )
                            )
                            
                        } else {
                        
                            if URL.init(string: "http://astalem.com:8080/upload/\(msg.message!)") != nil {
                                                               
                                let data = try Data.init(contentsOf: URL.init(string: "http://astalem.com:8080/upload/\(msg.message!)")!)
                                
                                var img_url = URL.init(string: "http://astalem.com:8080/upload/\(msg.message!)")
                                let image: UIImage = UIImage(data: data)! // UIImage(data: data)!
                                
                                var photo:Media = Media(
                                    url: nil,
                                    image: image ,
                                    placeholderImage: UIImage() ,
                                    size: CGSize(width: image.size.width, height: image.size.height)
                                )
                                                                
                                var user:Sender!
                                if msg.userPhone! == self.current_user_id {
                                    user = self.current_user
                                }else{
                                    user = self.other_user
                                }
                                
                                self.messages.append(
                                    Message(
                                        sender: user,
                                        messageId: "\(msg.id!)",
                                        sentDate: Date().addingTimeInterval(-86400),
                                        kind: .photo(photo)
                                    )
                                )
                            }
                        }
                        
                    }
                    
                    DispatchQueue.main.async {
                        // MARK: 2 disable blocking UI when loading messages
                        //loading.dismiss(animated: true, completion: nil)
                        self.title = "Chat"
                        self.messagesCollectionView.reloadData()
                        self.messagesCollectionView.scrollToBottom(animated: true)
                    }
                    
                }catch{
                    print("error in json \(error)" )
                }
                
             }.resume()
        }
        
    }
    
    // upload image to server
    func upload_photo_to_server(){
        DispatchQueue.global(qos: .background).async {
            
            var user_image_file: Data!
            if self.user_image != nil{
                user_image_file = self.user_image.jpegData(compressionQuality: 0.5)
            }else{
                return
            }
            
            let boundary = "Boundary-\(UUID().uuidString)"

            var request = URLRequest(url: URL(string: self.upload_image)!)
            request.httpMethod = "POST"
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            let httpBody = NSMutableData()
            httpBody.append(
                self.convertFileData(
                    fieldName: "image",
                    fileName: "imagename.png",
                    mimeType: "image/png",
                    fileData: user_image_file,
                    using: boundary))

            httpBody.appendString("--\(boundary)--")

            request.httpBody = httpBody as Data

            print(String(data: httpBody as Data, encoding: .utf8))
            
            URLSession.shared.dataTask(with: request) { data, response, error in
              // handle the response here
                
                if(error != nil){
                    print("\(error!.localizedDescription)")
                }
                
                //print("response = \(response)")

                
                guard let responseData = data else {
                    //self.show_alert(title: "خطأ", message: "حدث خطأ غير متوقع, الرجاء المحاولة مرة أخرى")
                    print("no response data")
                    return
                }
                
                if let responseString = String(data: responseData, encoding: .utf8) {
                    //print("uploaded to: \(responseString)")
                }
                
                do{
                    
                    var servrRespnse: ServerResponseUploadImage = try JSONDecoder().decode(ServerResponseUploadImage.self, from: responseData)
                    
                    if servrRespnse.message != "No Image"{
                        self.iamgeName = servrRespnse.message!
                        // call function to send to sever
                        self.send_image_json_to_server(imageName: self.iamgeName)
                    }
                    
                }catch{
                    print("error in reading server message \(error)")
                }
                
            }.resume()
        }
    }
    
    func convertFormField(named name: String, value: String, using boundary: String) -> String {
      var fieldString = "--\(boundary)\r\n"
      fieldString += "Content-Disposition: form-data; name=\"\(name)\"\r\n"
      fieldString += "\r\n"
      fieldString += "\(value)\r\n"

      return fieldString
    }
    
    func convertFileData(fieldName: String, fileName: String, mimeType: String, fileData: Data, using boundary: String) -> Data {
      let data = NSMutableData()

      data.appendString("--\(boundary)\r\n")
      data.appendString("Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(fileName)\"\r\n")
      data.appendString("Content-Type: \(mimeType)\r\n\r\n")
      data.append(fileData)
      data.appendString("\r\n")

      return data as Data
    }
    
    @objc func pressed(sender: UIButton!) {
        print("button pressed")
        self.select_camera_or_photo()
    }
    
    func setAttachmentManager(active: Bool) {
        
        var myFirstButton = UIButton()
        //myFirstButton.setTitle("✸", for: .normal)
        myFirstButton.setImage(UIImage(named: "attach"), for: .normal)
        myFirstButton.setTitleColor(UIColor.blue, for: .normal)
        myFirstButton.addTarget(self, action: #selector(pressed(sender:)), for: .touchUpInside)

        let topStackView = messageInputBar.leftStackView// topStackView
        topStackView.insertArrangedSubview(myFirstButton, at: topStackView.arrangedSubviews.count)
        topStackView.layoutIfNeeded()
        
    }
    
    /**
     Simple Action Sheet
     - Show action sheet with title and alert message and actions
     */
    func select_camera_or_photo() {
        
        DispatchQueue.main.async {
            
            let alert = UIAlertController(title: "Select", message: "Select Image", preferredStyle: .actionSheet)
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (_) in
                    
                    self.imagePcker.delegate = self
                    self.imagePcker.mediaTypes = ["public.image"]
                    self.imagePcker.allowsEditing = false
                    self.imagePcker.sourceType = .camera
                    self.present(self.imagePcker , animated: true , completion: nil)
                }))
            }
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
                alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (_) in
                   
                    self.imagePcker.delegate = self
                    self.imagePcker.mediaTypes = ["public.image"]
                    self.imagePcker.allowsEditing = false
                    self.imagePcker.sourceType = .photoLibrary
                    self.present(self.imagePcker , animated: true , completion: nil)
                }))
            }
            
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.savedPhotosAlbum) {
                alert.addAction(UIAlertAction(title: "Saved Album" , style: .default, handler: { (_) in
                    
                    self.imagePcker.delegate = self
                    self.imagePcker.mediaTypes = ["public.image"]
                    self.imagePcker.allowsEditing = false
                    self.imagePcker.sourceType = .savedPhotosAlbum
                    self.present(self.imagePcker , animated: true , completion: nil)
                }))
            }
            
            
            alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: { (_) in
                print("User click cancel button")
            }))
            
            self.present(alert, animated: true, completion: {
                print("completion block")
            })
            
        }
        
        
        
    }

    // this is listening to your photo select
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        dismiss(animated: true, completion: {
            
            print("img selected")
            let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            if(pickedImage == nil){ return; }
            self.user_image = pickedImage?.fixedOrientation()
            
            // upload it to server
            //self.upload_image_to_server()
            self.upload_photo_to_server()
            
        })
        
        
    }
    
}


class ChatHubConnectionDelegate: HubConnectionDelegate {

    weak var controller: ChatViewController?

    init(controller: ChatViewController) {
        self.controller = controller
    }

    func connectionDidOpen(hubConnection: HubConnection) {
        controller?.connectionDidOpen()
    }

    func connectionDidFailToOpen(error: Error) {
        controller?.connectionDidFailToOpen(error: error)
    }

    func connectionDidClose(error: Error?) {
        controller?.connectionDidClose(error: error)
    }

    func connectionWillReconnect(error: Error) {
        controller?.connectionWillReconnect(error: error)
    }

    func connectionDidReconnect() {
        controller?.connectionDidReconnect()
    }
}


extension UIImage {
    
    /// Fix image orientaton to protrait up
    func fixedOrientation() -> UIImage? {
        
        guard imageOrientation != UIImage.Orientation.up else {
            // This is default orientation, don't need to do anything
            return self.copy() as? UIImage
        }
        
        guard let cgImage = self.cgImage else {
            // CGImage is not available
            return nil
        }
        
        guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
            return nil // Not able to create CGContext
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2.0)
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat.pi / -2.0)
        case .up, .upMirrored:
            break
        @unknown default:
            break
        }
        
        // Flip image one more time if needed to, this is to prevent flipped image
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        @unknown default:
            break
        }
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        guard let newCGImage = ctx.makeImage() else { return nil }
        return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
    }



}

extension NSMutableData {
  func appendString(_ string: String) {
    if let data = string.data(using: .utf8) {
      self.append(data)
    }
  }
}

